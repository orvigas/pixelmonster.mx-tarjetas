(function () {

    angular.module('app', ['ngMessages']);
    angular.module('app').controller("WizardController", ['$scope', '$http', '$rootScope', wizardController]);

    angular.module('app').filter('range', function () {
        return function (input, min, max) {
            min = parseInt(min); //Make string input int
            max = parseInt(max);
            for (var i = min; i < max; i++)
                input.push(i);
            return input;
        };
    });

    /**Validación de input file**/
    angular.module('app').directive('validFile', function () {
        return {
            require: 'ngModel',
            link: function (scope, el, attrs, ngModel) {
                el.bind('change', function () {
                    scope.$apply(function () {
                        ngModel.$setViewValue(el.val());
                        ngModel.$render();
                    });
                });
            }
        };
    });

    /**Validar extensión del archivo**/
    angular.module('app').directive('validFormat', function () {
        var validFormats = ['ai', 'psd', 'png'];
        return {
            require: 'ngModel',
            link: function (scope, elem, attrs, ctrl, ngModel) {
                ctrl.$validators.extension = function (viewValue) {
                    return !viewValue || validFormats.indexOf(viewValue.substring(viewValue.lastIndexOf('.') + 1).toLowerCase()) !== -1;
                };
            }
        };
    });


    function wizardController($scope, $http, $rootScope) {
        //Model
        $scope.currentStep = 1;
        $scope.steps = [
            {
                step: 1,
                name: "Cotizador",
                template: "./partials/step1.html"
            },
            {
                step: 2,
                name: "Datos",
                template: "./partials/step2.html"
            },
            {
                step: 3,
                name: "Diseño",
                template: "./partials/step3.html"
            },
            {
                step: 4,
                name: "Envío",
                template: "./partials/step4.html"
            },
            {
                step: 5,
                name: "Pago",
                template: "./partials/step5.html"
            }
        ];
        /**Función para inicializar las variables**/

        $scope.config = {};
        $http.get('../sistema-tarjetas/scripts/config/config.json').success(function (data) {
            $scope.config = data;
        });
        $scope.items = {};
        $http.get('../sistema-tarjetas/scripts/config/items.php').success(function (data) {
            $scope.items = data;
        });


        $scope.init = function () {
            $scope.loading = false;
            /**Terminos y condiciones**/
            $scope.acceptTerms = {val: false};
            /**Compra**/
            $scope.buy = {
                "quantity": '',
                "cardfinish": "50% opaco",
                "sets": 1,
                "deliverytime": '',
                "iva": 0,
                "subtotal": 0,
                "total": 0,
                "bill": false,
                "deliveryprice": 0,
                "needdelivery": true,
                "card_content": {
                    "name": "",
                    "address": "",
                    "company": "",
                    "charge": "",
                    "office_phone": "",
                    "cell_phone": "",
                    "facebook": "",
                    "twitter": "",
                    "instagram": "",
                    "email": "",
                    "comments": "",
                    "needdesign": "0",
                    "template": null,
                    "logo": null
                },
                "delivery": {
                    "person": "",
                    "address": "",
                    "colony": "",
                    "city": "",
                    "state": "",
                    "postcode": "",
                    "phone": "",
                    "reference": ""
                }

            };
//            $http.get('../sistema-tarjetas/scripts/config/client.json').success(function (data) {
//                $scope.buyer = data;
//                $scope.buy = data.buy;
//                $scope.buy.iva = $scope.buy.subtotal * $scope.buy.tasa_iva;
//                $scope.buy.total = (parseFloat($scope.buy.iva) + parseFloat($scope.buy.subtotal));
//                $scope.calculateAmounts();
//            });
            /**Comprador**/
            $scope.buyer = {
                "nombre": "",
                "direccion": "",
                "telefono": "",
                "correo": ""
            };
            $scope.canProcess = false;
        };

        //Functions
        $scope.gotoStep = function (newStep) {
            $scope.currentStep = newStep;
        };
        $scope.getStepTemplate = function () {
            for (var i = 0; i < $scope.steps.length; i++) {
                if ($scope.currentStep == $scope.steps[i].step) {
                    ///Eliminar
//                    $scope.buy.subtotal = 1;
//                    $scope.buy.iva = 0;
//                    $scope.buy.total = 1;
//                    $scope.buy.deliveryprice = 0;
                    ///Eliminar
                    return $scope.steps[i].template;
                }
            }
        };
        $scope.item_number = 0;

        /**Calculate transaction amount**/

        $scope.calculateAmounts = function () {
            if ($scope.buy.item) {
                if ($scope.buy.item != 1) {
                    $scope.buy.sets = 1;
                }
                var $item = $("#" + $scope.buy.item + "-item");
                $scope.buy.subtotal = $item.data('price') * $scope.buy.sets;
                $scope.buy.deliverytime = $item.data('deliverytime');
                $scope.buy.deliveryprice = $item.data('deliveryprice');
                $scope.buy.quantity = $item.data('quantity');
                /**Variantes de excel de precios**/
                switch ($scope.buy.sets) {
                    case 3:
                        $scope.buy.deliverytime = '8 días hábiles';
                        $scope.buy.deliveryprice = 195;
                        break;
                    case 4:
                        $scope.buy.deliverytime = '9 días hábiles';
                        $scope.buy.deliveryprice = 215;
                        break;
                    case 4:
                        $scope.buy.deliverytime = '10 días hábiles';
                        $scope.buy.deliveryprice = 225;
                        break;
                    case 6:
                        $scope.buy.subtotal = 4840;
                        $scope.buy.deliverytime = '10 días hábiles';
                        $scope.buy.deliveryprice = 225;
                        break;
                    case 7:
                        $scope.buy.subtotal = 5645;
                        $scope.buy.deliverytime = '10 días hábiles';
                        $scope.buy.deliveryprice = 225;
                        break;
                    case 8:
                        $scope.buy.subtotal = 6200;
                        $scope.buy.deliverytime = '12 días hábiles';
                        $scope.buy.deliveryprice = 300;
                        break;
                    case 9:
                        $scope.buy.subtotal = 6955;
                        $scope.buy.deliverytime = '12 días hábiles';
                        $scope.buy.deliveryprice = 300;
                        break;
                    case 10:
                        $scope.buy.subtotal = 7605;
                        $scope.buy.deliverytime = '12 días hábiles';
                        $scope.buy.deliveryprice = 350;
                        break;
                    default:
                        break;
                }
                if ($scope.buy.bill) {
                    $scope.buy.iva = $scope.buy.subtotal * 0.16;
                } else {
                    $scope.buy.iva = 0;
                }
                if (!$scope.buy.needdelivery) {
                    $scope.buy.deliveryprice = 0;
                    $scope.buy.delivery = {
                        "person": "N/A",
                        "address": "N/A",
                        "colony": "N/A",
                        "city": "N/A",
                        "state": "N/A",
                        "postcode": "N/A",
                        "phone": "N/A",
                        "reference": "N/A"
                    };
                }

                $scope.buy.total = ($scope.buy.subtotal + $scope.buy.deliveryprice + $scope.buy.iva);
            } else {
                $scope.buy.subtotal = 0;
                $scope.buy.deliverytime = '';
                $scope.buy.deliveryprice = 0;
                $scope.buy.iva = 0;
                $scope.buy.total = 0;
            }
        };

        /**Guardar y hacer post**/
        $scope.save = function () {
            $scope.buy.card_content.file = null;
            var fd = new FormData();
            jQuery("input[type=file]").each(function () {
                fd.append('file', this.files[0]);
            });
            $http.post('../sistema-tarjetas/scripts/paypal/process_payment.php', {buy: $scope.buy, buyer: $scope.buyer})
                    .success(function (data, status, headers, config) {
                        fd.append('transaction', data.transaction.id);
                        uploadFile(fd);//Almacena archivos
                        jQuery('#item_number').val(data.transaction.id);
                        $scope.loading = true;
                        jQuery('#enviar').trigger('click');
                        return true;
                    });
            return false;
        };
        /**Limpiar archivos**/
        $scope.clearFile = function () {
            $("#file-container").empty();
            $scope.buy.card_content.template = '';
            $scope.buy.card_content.logo = '';
        };
    }
})();

function popover(obj) {
    var image = '<img src="./images/examples/' + $(obj).data('img') + '" class="img-responsive" style="width:250px; height:auto;">';
    $(obj).popover({
        placement: 'top',
        content: image,
        html: true
    });
}

/**variables para la subida de archivos**/
function getfile(obj) {
    var $this = $(obj), $clone = $this.clone();
    $this.attr('id', 'fileUpload-' + $clone.attr('id'));
    $this.attr('name', 'fileUpload-' + $clone.attr('id'));
    $this.removeAttr('ng-model');
    $this.removeAttr('onchange');
    $this.trigger('change');
    $("#file-container").empty();
    $this.after($clone).appendTo($("#file-container"));
}

function uploadFile(data) {
    jQuery.ajax({
        url: '../sistema-tarjetas/scripts/paypal/fileupload.php',
        data: data,
        cache: false,
        contentType: false,
        processData: false,
        type: 'POST',
        success: function (data) {
            return true;
        }
    });
    return false;
}

/**Funcion para desplegar el modal**/
function getTrans(id) {
    jQuery.ajax({
        url: '../sistema-tarjetas/scripts/database/getTransaction.php?id=' + id,
        type: 'GET',
        success: function (data) {
            fillModal(JSON.parse(data));
        }
    });
}

function fillModal(obj) {
    var HTML = "<div class='row'>" +
            "<br><div class='col-md-12' style='width:100%;'>" +
            "<h4>Datos de la operación</h4>" +
            "<table style='width:100%;' class='table table-bordered table-condensed table-hover table-striped'>" +
            "<tr>" +
            "<td class='col-md-2'><strong>Producto</strong></td><td>" + obj.transaction.item + "</td>" +
            "</tr>" +
            "<tr>" +
            "<td class='col-md-2'><strong>Cantidad</strong></td><td>" + (obj.transaction.sets * obj.it.quantity) + "</td>" +
            "</tr>" +
            "<tr>" +
            "<td class='col-md-2'><strong>Juegos</strong></td><td>" + obj.transaction.sets + "</td>" +
            "</tr>" +
            "<tr>" +
            "<td class='col-md-2'><strong>Acabado</strong></td><td>" + obj.transaction.finish + "</td>" +
            "</tr>" +
            "<tr>" +
            "<td class='col-md-2'><strong>Sub Total</strong></td><td>$" + parseFloat(obj.transaction.subtotal).toFixed(2) + "</td>" +
            "</tr>" +
            "<tr>" +
            "<td class='col-md-2'><strong>Envío</strong></td><td>$" + parseFloat(obj.it.delivery_price).toFixed(2) + "</td>" +
            "</tr>" +
            "<tr>" +
            "<td class='col-md-2'><strong>IVA</strong></td><td>$" + parseFloat(obj.transaction.iva).toFixed(2) + "</td>" +
            "</tr>" +
            "<tr>" +
            "<td class='col-md-2'><strong>TOTAL</strong></td><td><strong>$" + parseFloat(obj.transaction.iva + obj.it.delivery_price + obj.transaction.subtotal).toFixed(2) + "</strong></td>" +
            "</tr>" +
            "<tr>" +
            "<td class='col-md-2'><strong>Comprador</strong></td><td>" + obj.buyer.name + "</td>" +
            "</tr>" +
            "<tr>" +
            "<td class='col-md-2'><strong>Dirección</strong></td><td>" + obj.buyer.address + "</td>" +
            "</tr>" +
            "<tr>" +
            "<td class='col-md-2'><strong>Correo</strong></td><td>" + obj.buyer.email + "</td>" +
            "</tr>" +
            "<tr>" +
            "<td class='col-md-2'><strong>Telefono</strong></td><td>" + obj.buyer.phone + "</td>" +
            "</tr>" +
            "</table>" +
            "</div>" +
            needDesign(obj) +
            needDelivery(obj) +
            "</div><br><br>";
    jQuery("#modal-title").html("Operación: <strong>" + obj.pr.tx + "</strong>");
    jQuery("#modal-body").html(HTML);
    jQuery("#modal-footer").emty();
}

function needDesign(obj) {
    var html = "";
    if (parseInt(obj.design.need_design)) {
        html = "<br><div class='col-md-12' style='width:100%;'>" +
                "<h4>Contenido de las tarjetas</h4>" +
                "<table style='width:100%;' class='table table-bordered table-condensed table-hover table-striped'>" +
                "<tr>" +
                "<td class='col-md-2'><strong>Nombre</strong></td><td>" + obj.design.name + "</td>" +
                "</tr>" +
                "<tr>" +
                "<td class='col-md-2'><strong>Dirección</strong></td><td>" + obj.design.address + "</td>" +
                "</tr>" +
                "<tr>" +
                "<td class='col-md-2'><strong>Compañia</strong></td><td>" + obj.design.company_name + "</td>" +
                "</tr>" +
                "<tr>" +
                "<td class='col-md-2'><strong>Cargo</strong></td><td>" + obj.design.charge + "</td>" +
                "</tr>" +
                "<tr>" +
                "<td class='col-md-2'><strong>Telefóno</strong></td><td>" + obj.design.phone + "</td>" +
                "</tr>" +
                "<tr>" +
                "<td class='col-md-2'><strong>Celular</strong></td><td>" + obj.design.cellphone + "</td>" +
                "</tr>" +
                "<tr>" +
                "<td class='col-md-2'><strong>Facebook</strong></td><td>" + obj.design.facebook + "</td>" +
                "</tr>" +
                "<tr>" +
                "<td class='col-md-2'><strong>Twitter</strong></td><td>" + obj.design.twitter + "</td>" +
                "</tr>" +
                "<tr>" +
                "<td class='col-md-2'><strong>Instagram</strong></td><td>" + obj.design.instagram + "</td>" +
                "</tr>" +
                "<tr>" +
                "<td class='col-md-2'><strong>Correo</strong></td><td>" + obj.design.email + "</td>" +
                "</tr>" +
                "<tr>" +
                "<td class='col-md-2'><strong>Comentarios</strong></td><td>" + obj.design.comments + "</td>" +
                "</tr>" +
                "</table>" +
                "</div>";
    }
    return html;
}

function needDelivery(obj) {
    var html = "";
    if (parseInt(obj.delivery.needdelivery)) {
        html = "<br><div class='col-md-12' style='width:100%;'>" +
                "<h4>Datos de envío</h4>" +
                "<table style='width:100%;' class='table table-bordered table-condensed table-hover table-striped'>" +
                "<tr>" +
                "<td class='col-md-2'><strong>Nombre</strong></td><td>" + obj.delivery.name + "</td>" +
                "</tr>" +
                "<tr>" +
                "<td class='col-md-2'><strong>Dirección</strong></td><td>" + obj.delivery.address + "</td>" +
                "</tr>" +
                "<tr>" +
                "<td class='col-md-2'><strong>Colonia</strong></td><td>" + obj.delivery.colony + "</td>" +
                "</tr>" +
                "<tr>" +
                "<td class='col-md-2'><strong>Ciudad o Municipio</strong></td><td>" + obj.delivery.city + "</td>" +
                "</tr>" +
                "<tr>" +
                "<td class='col-md-2'><strong>Estado</strong></td><td>" + obj.delivery.state + "</td>" +
                "</tr>" +
                "<tr>" +
                "<td class='col-md-2'><strong>CP.</strong></td><td>" + obj.delivery.postcode + "</td>" +
                "</tr>" +
                "<tr>" +
                "<td class='col-md-2'><strong>Teléfono</strong></td><td>" + obj.delivery.phone + "</td>" +
                "</tr>" +
                "</table>" +
                "</div>";
    }
    return html;
}


/**Habilita la edicion de costos**/
function showSave(obj) {
    if (obj.checked) {
        $("#show-row-" + obj.value).hide();
        $("#edit-row-" + obj.value).show();
        obj.checked = false;
    } else {
        $("#show-row-" + obj.value).show();
        $("#edit-row-" + obj.value).hide();
        obj.checked = true;
    }
}
 