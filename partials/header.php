<?php
session_start();
require $_SESSION["base_path"] . "scripts/autoload.php";
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <base href="<?php echo "http://$_SERVER[HTTP_HOST]"; ?>/sistema-tarjetas/">
        <link href="css/bootstrap.css" rel="stylesheet" data-semver="3.3.6"
              data-require="bootstrap-css@3.3.6"/>
        <link data-require="fontawesome@4.5.0" data-semver="4.5.0" rel="stylesheet"
              href="css/font-awesome.css"/>
        <script data-require="angular.js@1.4.8" data-semver="1.4.8"
        src="js/angular.js"></script>
        <script data-require="angular.js@1.4.8" data-semver="1.4.8"
        src="js/angular-messages.js"></script>
        <script src="js/jquery-2.1.4.js" data-semver="2.1.4" data-require="jquery@*"></script>
        <script src="js/bootstrap.min.js" data-semver="3.3.6"
        data-require="bootstrap@3.3.6"></script>
        <link href="css/style.css" rel="stylesheet"/>
        <link href="css/nav-wizard.bootstrap.css" rel="stylesheet"/>
        <link rel="shortcut icon" href="images/favicon/favicon-16x16.png"/>
        <script src="js/script.js?cache=<?php echo rand(10, 99); ?>"></script>
        <style>
            @import url('https://fonts.googleapis.com/css?family=Source+Sans+Pro');
            body{
                font-family: 'Source Sans Pro', sans-serif;
            }
        </style>
    </head>
    <body ng-app="app" ng-controller="WizardController">
        <div class="container-fluid">