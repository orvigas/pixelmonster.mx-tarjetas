<?php
/**
 * Created by PhpStorm.
 * User: orlando
 * Date: 30/09/2016
 * Time: 03:54 PM
 */
session_start();
$_SESSION["base_path"] = $_SERVER['DOCUMENT_ROOT'] . "/sistema-tarjetas/"; //BasePath modificar para poner en producción
$_SESSION["base_url"] = "http://$_SERVER[HTTP_HOST]/sistema-tarjetas/";
require_once $_SESSION["base_path"] . "partials/header.php";
$db = new TransactionDB();
$transactions = $db->getTransactions();
$items = $db->getItems();
$paypal = $db->getPaypalAccount();
/* * Inicio de sesión* */
$db->close();
?>
<style type="text/css">
    body{
        padding:10px;
    }
    .img-admin-logo{
        max-width: 100px;
        height: auto;
        margin-top: -8px;
    }
    .page-admin-title{
        margin-top: 20px!important;
    }
</style>
<br><br><br><br>
<!-- Fixed navbar -->
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="<?php echo "http://$_SERVER[HTTP_HOST]"; ?>">
                <img class="img-admin-logo" src="<?php echo "http://$_SERVER[HTTP_HOST]"; ?>/sistema-tarjetas/images/logo_pixel.png" alt="Brand"/>
            </a>
        </div>
        <ul class="nav navbar-nav navbar-right">
            <?php if (isset($_SESSION['user'])) { ?>
                <li><a href="<?php echo "http://$_SERVER[HTTP_HOST]"; ?>/sistema-tarjetas/transactionViewer/">Módulo de administración de ventas</a></li>
                <li><a href="<?php echo "http://$_SERVER[HTTP_HOST]"; ?>/sistema-tarjetas/scripts/tools/logout.php">Salir <span class="glyphicon glyphicon-log-out" aria-hidden="true"></span></a></li>
            <?php } ?>
        </ul>
    </div>
</nav>

<div class="container-fluid">
    <?php if (isset($_SESSION['user'])) { ?>
        <div class="row">
            <div class="col-md-12 text-center">
                <h3>Administración de ventas</h3>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-12 text-center">
                <a class="btn btn-primary" role="button" data-toggle="collapse" href="#operations" aria-expanded="false" aria-controls="collapseExample">
                    <span class="glyphicon glyphicon-usd" aria-hidden="true"></span>&nbsp;&nbsp;Consulta de operaciones
                </a>
                <a class="btn btn-primary" role="button" data-toggle="collapse" href="#catalogs" aria-expanded="false" aria-controls="collapseExample">
                    <span class="glyphicon glyphicon-th-list" aria-hidden="true"></span>&nbsp;&nbsp;Administración de catálogos
                </a>
                <a class="btn btn-primary" role="button" data-toggle="collapse" href="#config" aria-expanded="false" aria-controls="collapseExample">
                    <span class="glyphicon glyphicon-tags" aria-hidden="true"></span>&nbsp;&nbsp;Parámetros generales
                </a>
            </div>
        </div>
        <div class="collapse" id="operations">
            <h1>Operaciones</h1>
            <div class="col-md-12">
                <br>
                <table class="table table-bordered table-condensed table-hover table-striped">
                    <thead>
                        <tr>
                            <th>No.</th><th>Cliente</th><th>Correo</th><th>Ref. PayPal</th><th>Fecha</th><th>Estado de la operación</th><th>Detalles</th><th>Archivos</th><th>Imprimir</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($transactions as $trans) { ?>
                            <tr>
                                <td class="text-center"><?php echo $trans['transaction_id'] ?></td>
                                <td class="text-left"><?php echo $trans['buyer_name'] ?></td>
                                <td class="text-left"><?php echo $trans['buyer_email'] ?></td>
                                <td class="text-center"><?php echo $trans['tx'] ?></td>
                                <td class="text-right"><?php echo $trans['created_at'] ?></td>
                                <td class="text-center"><?php echo $trans['st'] ?></td>
                                <td class="text-center"><button class="btn btn-link btn-sm" data-toggle="modal" data-target="#myModal" onclick="getTrans('<?php echo $trans['transaction_id'] ?>')"><span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span> Detalle</button></td>
                                <td class="text-center"><a class="btn btn-link btn-sm" target="_blank" href="<?php echo "http://$_SERVER[HTTP_HOST]"; ?>/sistema-tarjetas/scripts/tools/zip.php?folder=<?php echo str_pad($trans['transaction_id'], 9, '0', STR_PAD_LEFT); ?>"><span class="glyphicon glyphicon-save" aria-hidden="true"></span> Archivos</a></td>
                                <td><button class="btn btn-link btn-sm" onclick="printComp('<?php echo $trans['transaction_id'] ?>')"><i class="fa fa-fw fa-file-pdf-o"></i> Descargar</button></td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="collapse" id="catalogs">
            <h1>Catálogos</h1>
            <br>
            <div class="col-md-12">
                <h3>Costos</h3>
                <table class="table table-bordered table-condensed table-hover table-striped">
                    <thead>
                        <tr>
                            <th>No.</th><th>Cantidad</th><th>Precio</th><th>Costo envío</th><th>Tiempo de entrega</th><th class="text-center">Editar</th><th class="text-center">Guardar</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($items as $item) { ?>
                        <form method="post" action="<?php echo "http://$_SERVER[HTTP_HOST]"; ?>/sistema-tarjetas/scripts/database/updateCosto.php">
                            <tr id="show-row-<?php echo $item['id'] ?>">
                                <td class="text-center"><?php echo $item['id'] ?></td>
                                <td class="text-center"><?php echo $item['quantity'] ?></td>
                                <td class="text-center">$<?php echo number_format($item['price'], 2) ?></td>
                                <td class="text-center">$<?php echo number_format($item['delivery_price'], 2) ?></td>
                                <td class="text-center"><?php echo $item['delivery_time'] ?></td>
                                <td class="text-center"><input type="checkbox" name="item-<?php echo $item['id'] ?>" onchange="showSave(this)" value="<?php echo $item['id'] ?>" value="<?php echo $item['id'] ?>"></td>
                                <td class="text-center"><span class="btn btn-success btn-sm disabled">Guardar</span></td>
                            </tr>
                            <tr id="edit-row-<?php echo $item['id'] ?>" style="display:none;">
                                <td class="text-center"><?php echo $item['id'] ?><input type="hidden" name="id" value="<?php echo $item['id'] ?>"/></td>
                                <td><input type="text" class="form-control" name="quantity" value="<?php echo $item['quantity'] ?>"/></td>
                                <td><input type="text" class="form-control" name="price" value="<?php echo $item['price'] ?>"/></td>
                                <td><input type="text" class="form-control" name="delivery_price" value="<?php echo $item['delivery_price'] ?>"/></td>
                                <td><input type="text" class="form-control" name="delivery_time" value="<?php echo $item['delivery_time'] ?>"/></td>
                                <td class="text-center"><input type="checkbox" name="item-edit-<?php echo $item['id'] ?>" checked="checked" onchange="showSave(this)" value="<?php echo $item['id'] ?> "></td>
                                <td class="text-center"><button type="submit" class="btn btn-success btn-sm">Guardar</button></td>
                            </tr>
                        </form>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="collapse" id="config">
            <h1>Parámetros generales</h1>
            <br>
            <div class="col-md-12">
                <form class="form-horizontal" method="post" action="<?php echo "http://$_SERVER[HTTP_HOST]"; ?>/sistema-tarjetas/scripts/database/updatePaypal.php">
                    <input type="hidden" name="id" value="<?php echo $paypal['id']; ?>"/>
                    <div class="form-group">
                        <label for="email">Correo electrónico</label>
                        <input type="mail" class="form-control" id="email" name="email" placeholder="algo@algo.com" value="<?php echo $paypal['email']; ?>">
                    </div>
                    <div class="form-group">
                        <label for="url">URL</label>
                        <input type="text" class="form-control" id="url" name="url" placeholder="https://paypal.com/webervice" value="<?php echo $paypal['url']; ?>">
                    </div>
                    <div class="form-group">
                        <label for="comments">Comentarios</label>
                        <input type="text" class="form-control" id="comments" name="comments" placeholder="Aquí sus comentarios" value="<?php echo $paypal['comments']; ?>">
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-default">Guardar cambios</button>
                    </div>
                </form>
            </div>
        </div>
    <?php } else { ?>
        <div class="row">
            <div class="col-md-12">
                <br><br><br><hr>
                <div class="col-md-4"></div>
                <div class="col-md-4">
                    <form class="form-signin" method="post" action="<?php echo "http://$_SERVER[HTTP_HOST]"; ?>/sistema-tarjetas/scripts/tools/login.php">
                        <h2 class="form-signin-heading text-orange"><strong>Iniciar sesión</strong></h2>
                        <label for="inputUser" class="sr-only">Usuario</label>
                        <input type="text" id="inputUser" name="user" class="form-control" placeholder="Usuario" required="true" autofocus="">
                        <br>
                        <label for="inputPassword" class="sr-only">Contraseña</label>
                        <input type="password" id="inputPassword" name="pass" class="form-control" placeholder="Contraseña" required="">
                        <br>
                        <button class="btn btn-lg btn-primary btn-block" type="submit">Iniciar sesión</button>
                    </form>
                </div>
                <div class="col-md-4"></div>
                <hr>
            </div>            
        </div>
    <?php } ?>

</div> <!-- /container -->
<div class="modal fade bs-example-modal-lg" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="modal-title" id="myModalLabel">Modal title</h4>
            </div>
            <div class="modal-body" id="modal-body">
                ...
            </div>
            <div class="modal-footer hidden" id="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function printComp(obj) {
        window.open('<?php echo $_SESSION['base_url']; ?>scripts/paypal/pdf.php?transaction=' + obj, 'Pedido No.' + obj, "height=800,width=600,location=0,scrollbars=1,toolbar=0");
        return 0;
    }
</script>
<?php
require_once $_SESSION["base_path"] . "partials/footer.php";
?>
