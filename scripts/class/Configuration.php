<?php

/**
 * Created by PhpStorm.
 * User: orlando
 * Date: 21/09/2016
 * Time: 03:42 PM
 */
class Configuration {

    var $client;
    var $config;
    var $items;

    function Configuration() {
        $this->client = json_decode(file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/sistema-tarjetas/scripts/config/client.json', "r"));
        $this->config = json_decode(file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/sistema-tarjetas/scripts/config/config.json', "r"));
        $this->items = json_decode(file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/sistema-tarjetas/scripts/config/items.php', "r"));
    }

}
