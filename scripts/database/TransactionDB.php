<?php

/**
 * Created by PhpStorm.
 * User: orlando
 * Date: 03/10/2016
 * Time: 03:09 PM
 */
class TransactionDB extends SQLite3 {

    function __construct() {
        $this->open($_SESSION["base_path"] . "scripts/database/transaction.db");
    }

    /*     * Bloque de Transacciones* */

    public function getTransactions() {
        $return = array();
        $result = $this->query('SELECT A.id AS "transaction_id" ,E.name AS "buyer_name",E.email AS "buyer_email",* FROM "transaction" A LEFT JOIN "delivery" B ON A.id = B.transaction_id LEFT JOIN "design" C ON A.id = C.transaction_id LEFT JOIN "payment_reference" D ON A.id = D.transaction_id LEFT JOIN "buyer" E ON A.id = E.transaction_id LEFT JOIN "item" F ON A.item_id = F.id WHERE A.status = 1 AND datetime(A.created_at) >= datetime("now", "-90 days") AND length(D.tx) > 0 ORDER BY A.id DESC');
        while ($row = $result->fetchArray(SQLITE3_ASSOC)) {
            array_push($return, $row);
        }
        return $return;
    }

    public function getTransaction($id) {
        $tr = $this->querySingle("SELECT * FROM 'transaction' WHERE id =" . $id . " AND status = 1", true); //transaction
        $by = $this->querySingle("SELECT * FROM 'buyer' WHERE transaction_id =" . $id . " AND status = 1", true); //Buyer
        $dl = $this->querySingle("SELECT * FROM 'delivery' WHERE transaction_id =" . $id . " AND status = 1", true); //Delivery
        $ds = $this->querySingle("SELECT * FROM 'design' WHERE transaction_id =" . $id . " AND status = 1", true); //Design
        $pr = $this->querySingle("SELECT * FROM 'payment_reference' WHERE transaction_id =" . $id, true); //Payment reference
        $it = $this->querySingle("SELECT * FROM 'item' WHERE id =" . $tr['item_id'], true); //Payment reference
        return ['transaction' => $tr, "buyer" => $by, "delivery" => $dl, "design" => $ds, 'pr' => $pr, 'it' => $it];
    }

    public function insertTransaction($item, $item_id, $sets, $finish, $subtotal, $iva, $deliveryprice) {
        $this->exec("INSERT INTO main.'transaction' ('item','sets','finish','subtotal','iva','deliveryprice','created_at','item_id','status') VALUES ('" . $item . "', '" . $sets . "','" . $finish . "', '" . $subtotal . "', '" . $iva . "','" . $deliveryprice . "',datetime('now')," . $item_id . ",'1')");
        return $this->querySingle("SELECT * FROM 'transaction' A WHERE A.id = " . $this->lastInsertRowID(), true);
    }

    /*     * Bloque de Buyer* */

    public function insertBuyer($name, $address, $phone, $email, $transaction_id) {
        return $this->exec("INSERT INTO main.'buyer' ('name','address','phone','email','status','transaction_id') VALUES ('$name', '$address', '$phone', '$email','1',$transaction_id)");
    }

    /*     * Bloque de Design* */

    public function insertDesign($name, $address, $company_name, $charge, $phone_1, $phone_2, $cellphone, $facebook, $twitter, $instagram, $email, $comments, $needdesign, $transaction_id) {
        return $this->exec("INSERT INTO main.'design' ('name','address','company_name','charge','phone_1','phone_2','cellphone','facebook','twitter','instagram','email', 'comments','need_design','status','transaction_id') VALUES ('$name', '$address', '$company_name', '$charge', '$phone_1','$phone_2','$cellphone','$facebook','$twitter','$instagram','$email','$comments','$needdesign','1',$transaction_id)");
    }

    /*     * Bloque de Delivery* */

    public function insertDelivery($name, $address, $colony, $city, $state, $postcode, $phone, $references, $needdelivery, $transaction_id) {
        return $this->exec("INSERT INTO main.'delivery' ('name','address','colony','city','state','postcode','phone','references','needdelivery','status','transaction_id') VALUES ('$name', '$address', '$colony', '$city', '$state', '$postcode', '$phone', '$references', '$needdelivery','1',$transaction_id)");
    }

    /*     * Bloque payment reference* */

    public function insertPaymentReference($tx, $amt, $cc, $st, $transaction_id) {
        if (!$this->querySingle("SELECT * FROM 'payment_reference' WHERE transaction_id =" . $transaction_id, true)) {
            if ($this->exec("INSERT INTO main.'payment_reference' ('tx','amt','cc','st','transaction_id') VALUES ('$tx','$amt','$cc','$st',$transaction_id)")) {
                return $this->getTransaction($transaction_id);
            }
            return 0;
        } else {
            return $this->getTransaction($transaction_id);
        }
        return 0;
    }

    /*     * Inserción de catálogo "item"* */

    public function insertItems() {
        $this->exec("INSERT INTO main.'item' ('quantity','price','delivery_price','delivery_time','status') VALUES ('200','1300','175','7 días hábiles','1')");
        $this->exec("INSERT INTO main.'item' ('quantity','price','delivery_price','delivery_time','status') VALUES ('400','2015','175','7 días hábiles','1')");
        $this->exec("INSERT INTO main.'item' ('quantity','price','delivery_price','delivery_time','status') VALUES ('600','2695','195','8 días hábiles','1')");
        $this->exec("INSERT INTO main.'item' ('quantity','price','delivery_price','delivery_time','status') VALUES ('800','3440','215','9 días hábiles','1')");
        $this->exec("INSERT INTO main.'item' ('quantity','price','delivery_price','delivery_time','status') VALUES ('1000','3985','225','10 días hábiles','1')");
    }

    /*     * Actualiza costos* */

    public function updateCosto($id, $quantity, $price, $delivery_price, $delivery_time) {
        return $this->exec("UPDATE item SET quantity = '$quantity', price='$price', delivery_price = '$delivery_price', delivery_time = '$delivery_time', status = '1' WHERE id = '$id'");
    }

    /*     * Inserta Administrador* */

    public function insertAdmin() {
        $this->exec("INSERT INTO main.'user' ('name','username','password','status') VALUES('Administrador','pixel.admin','monster2ol6*','1')");
    }

    /*     * Inserta cuenta de paypal* */

    public function insertPaypal() {
        $this->exec("INSERT INTO main.'paypal_account' ('email','url','comments') VALUES ('info@pixelmonster.mx','https://www.paypal.com/cgi-bin/webscr',null)");
    }

    /*     * Obtinene la cuenta de paypal* */

    public function getPaypalAccount() {
        return $this->querySingle("SELECT * FROM 'paypal_account' ", true);
    }

    /*     * Actualiza cuenta* */

    public function updatePaypal($id, $email, $url, $comments) {
        return $this->exec("UPDATE paypal_account SET email = '$email', url='$url', comments='$comments' WHERE id = '$id'");
    }

    /*     * Login* */

    public function login($usr, $pass) {
        $usuario = $this->querySingle("SELECT * FROM 'user' WHERE username ='" . $usr . "'AND password ='" . $pass . "' AND status = 1", true); //usuario
        return $usuario;
    }

    /*     * Bloque de Items* */

    public function getItems() {
        $result = array();
        $queryResults = $this->query("SELECT * FROM 'item'");
        while ($row = $queryResults->fetchArray()) {
            array_push($result, ['id' => $row['0'], 'quantity' => $row['1'], 'price' => $row['2'], 'delivery_price' => $row['3'], 'delivery_time' => $row['4']]);
        }
        return $result;
    }

}
