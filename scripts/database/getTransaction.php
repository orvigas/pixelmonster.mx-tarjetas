<?php

session_start();
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

// Report simple running errors
error_reporting(0);
require $_SESSION["base_path"] . "scripts/autoload.php";
$db = new TransactionDB();
$transaction = $db->getTransaction($_GET['id']);
$transaction['folder'] = str_pad($_GET['id'], 9, '0', STR_PAD_LEFT);
$db->close();
echo json_encode($transaction, true);
