<?php

session_start();
/**
 * Created by PhpStorm.
 * User: orlando
 * Date: 03/10/2016
 * Time: 03:05 PM
 */
require "./TransactionDB.php";

/* * Incialización de la base de datos* */
$db = new TransactionDB();
if (!$db) {
    echo $db->lastErrorMsg();
} else {
    echo "Opened database successfully<br>";
}

/* * Tabla item* */
$sql = '
        CREATE TABLE "main"."user" (
        "id"  INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
        "name"  TEXT NOT NULL,
        "username" TEXT NOT NULL,
        "password"  TEXT NOT NULL,
        "status"  INTEGER
        )
';
$ret = $db->exec($sql);
$db->insertAdmin();
if (!$ret) {
    echo $db->lastErrorMsg();
} else {
    echo "Table user created successfully<br>";
}

/* * Tabla paypal* */
$sql = '
        CREATE TABLE "main"."paypal_account" (
        "id"  INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
        "email"  TEXT NOT NULL,
        "url"  TEXT NOT NULL,
        "comments" TEXT
        )
';
$ret = $db->exec($sql);
$db->insertPaypal();
if (!$ret) {
    echo $db->lastErrorMsg();
} else {
    echo "Table user created successfully<br>";
}

/* * Tabla item* */
$sql = '
        CREATE TABLE "main"."item" (
        "id"  INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
        "quantity"  INTEGER NOT NULL,
        "price" REAL NOT NULL,
        "delivery_price"  REAL NOT NULL,
        "delivery_time"  TEXT(255) NOT NULL,
        "status"  INTEGER
        )
';
$ret = $db->exec($sql);
$db->insertItems();
if (!$ret) {
    echo $db->lastErrorMsg();
} else {
    echo "Table item created successfully<br>";
}


/* * Tabla transaction* */
$sql = '
        CREATE TABLE "main"."transaction" (
        "id"  INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
        "item"  TEXT(255) NOT NULL,
        "sets"  INTEGER,
        "finish"  TEXT(255) NOT NULL,
        "subtotal"  REAL NOT NULL,
        "iva"  REAL,
        "deliveryprice"  REAL,
        "created_at"  TEXT(50) NOT NULL,
        "item_id"  INTEGER NOT NULL,
        "status"  INTEGER,
        CONSTRAINT "item_id" FOREIGN KEY ("item_id") REFERENCES "item" ("id")
        )
';
$ret = $db->exec($sql);
if (!$ret) {
    echo $db->lastErrorMsg();
} else {
    echo "Table transaction created successfully<br>";
}

/* * Tabla buyer* */
$sql = '
        CREATE TABLE "main"."buyer" (
        "id"  INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
        "name"  TEXT(255) NOT NULL,
        "address"  TEXT(255) NOT NULL,
        "phone"  TEXT(20),
        "email"  TEXT(50),
        "status"  INTEGER,
        "transaction_id"  INTEGER NOT NULL,
        CONSTRAINT "transaction_id" FOREIGN KEY ("transaction_id") REFERENCES "transaction" ("id")
        )
';
$ret = $db->exec($sql);
if (!$ret) {
    echo $db->lastErrorMsg();
} else {
    echo "Table buyer created successfully<br>";
}

/* * Tabla delivery* */
$sql = '
        CREATE TABLE "main"."delivery" (
        "id"  INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
        "name"  TEXT(255),
        "address"  TEXT(255),
        "colony"  TEXT(100),
        "city"  TEXT(100),
        "state"  TEXT(100),
        "postcode"  TEXT(15),
        "phone"  TEXT(20),
        "references"  TEXT(200),
        "needdelivery"  TEXT(10),
        "status"  INTEGER,
        "transaction_id"  INTEGER NOT NULL,
        CONSTRAINT "transaction_id" FOREIGN KEY ("transaction_id") REFERENCES "transaction" ("id")
        )
';
$ret = $db->exec($sql);
if (!$ret) {
    echo $db->lastErrorMsg();
} else {
    echo "Table delivery created successfully<br>";
}

/* * Tabla design* */
$sql = '
        CREATE TABLE "main"."design" (
        "id"  INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
        "name"  TEXT(255) NOT NULL,
        "address"  TEXT(255) NOT NULL,
        "company_name"  TEXT(255),
        "charge"  TEXT(255),
        "phone_1"  TEXT(25),
        "phone_2"  TEXT(25),
        "cellphone"  TEXT(25),
        "facebook"  TEXT(50),
        "twitter"  TEXT(50),
        "instagram"  TEXT(50),
        "email"  TEXT(50),
        "comments"  TEXT(255),
        "need_design" TEXT(10),
        "status"  INTEGER,
        "transaction_id"  INTEGER NOT NULL,
        CONSTRAINT "transaction_id" FOREIGN KEY ("transaction_id") REFERENCES "transaction" ("id")
        )
';
$ret = $db->exec($sql);
if (!$ret) {
    echo $db->lastErrorMsg();
} else {
    echo "Table design created successfully<br>";
}

/* * Tabla payment_reference* */
$sql = '
        CREATE TABLE "main"."payment_reference" (
        "tx"  TEXT(255),
        "amt"  TEXT(255),
        "cc"  TEXT(100),
        "st"  TEXT(100),
        "transaction_id"  INTEGER NOT NULL,
        CONSTRAINT "transaction_id" FOREIGN KEY ("transaction_id") REFERENCES "transaction" ("id")
        )
';
$ret = $db->exec($sql);
if (!$ret) {
    echo $db->lastErrorMsg();
} else {
    echo "Table payment reference created successfully<br>";
}
$db->exec('UPDATE "main"."sqlite_sequence" SET seq = 1 WHERE name = \'item\'');
$db->exec('UPDATE "main"."sqlite_sequence" SET seq = 1 WHERE name = \'paypal_account\'');
$db->exec('UPDATE "main"."sqlite_sequence" SET seq = 1 WHERE name = \'transaction\'');
$db->exec('UPDATE "main"."sqlite_sequence" SET seq = 1 WHERE name = \'delivery\'');
$db->exec('UPDATE "main"."sqlite_sequence" SET seq = 1 WHERE name = \'buyer\'');
$db->exec('UPDATE "main"."sqlite_sequence" SET seq = 1 WHERE name = \'design\'');


$db->close();
