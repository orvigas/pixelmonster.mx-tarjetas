<?php

/* * Variables super globales* */

date_default_timezone_set('America/Mexico_City');
session_start();
ini_set('allow_url_include', 'On');
$_SESSION["base_path"] = $_SERVER['DOCUMENT_ROOT'] . "/sistema-tarjetas/"; //BasePath modificar para poner en producción
$_SESSION["base_url"] = "http://$_SERVER[HTTP_HOST]/sistema-tarjetas/";
require $_SESSION["base_path"] . "scripts/class/Configuration.php";
require $_SESSION["base_path"] . "scripts/database/TransactionDB.php";
require $_SESSION["base_path"] . "scripts/tools/mailer.php";
$config = new Configuration();
$db = new TransactionDB();
/* * PAYPAL CONFIG* */
//Set useful variables for paypal form
//$paypal_url = 'https://www.sandbox.paypal.com/cgi-bin/webscr'; //Test PayPal API URL
//$paypal_id = 'orlando.villegas@live.com.mx'; //Business Email
$r = $db->getPaypalAccount();
$paypal_id = $r['email'];
$paypal_url = $r['url'];
$db->close();
