<?php

session_start();
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
// Report simple running errors
error_reporting(0);
require $_SESSION["base_path"] . "scripts/autoload.php";

if (!empty($_FILES['file']['name'])) {
    /*     * Crear directorio y subir archivo* */
    $upload_dir = $_SESSION["base_path"] . "transactionfiles/" . $_POST['transaction'] . "/";

    if (!file_exists($upload_dir)) {
        mkdir($upload_dir, 0777, true);  //create directory if not exist
    }

    $image_name = basename($_FILES['file']['name']);
    $image = time() . '_' . $image_name;
    move_uploaded_file($_FILES['file']['tmp_name'], $upload_dir . $image); // upload file
    echo "success";
} else {
    echo "no files";
}
