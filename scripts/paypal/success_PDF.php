<?php
session_start();
$_SESSION["base_path"] = $_SERVER['DOCUMENT_ROOT'] . "/sistema-tarjetas/"; //BasePath modificar para poner en producción
/**
 * Created by PhpStorm.
 * User: orlando
 * Date: 03/10/2016
 * Time: 04:40 PM
 */
require_once $_SESSION["base_path"] . "partials/header.php";
if (!(isset($_GET['transaction']))) {
    echo '<br><br><div class="col-md-12 text-center"><h1>¡Datos incorrectos!</h1></div>';
    return null;
}
$db = new TransactionDB();
$transaction = $db->getTransaction((integer) $_GET['transaction']);
?>
<style>
    body{
        font-size: 14px;
    }
    .table-title{
        width: 150px!important;
    }
    table{
        width: 100%;
        border-collapse: collapse;
    }
    table, th, td {
        border: 1px solid black;
    }
    th, td {
        padding-left: 5px;
        text-align: left;
    }
    .text-magenta{
        color: #c74f93;
    }

    .text-orange{
        color: #e68542;
    }
    strong{
        font-weight: bolder!important;
    }
</style>
<div class="row">
<!--    <img src="<?php echo "http://$_SERVER[HTTP_HOST]"; ?>/sistema-tarjetas/images/logo_pixel.png"
         style="float: right; width: 150px; height: auto; position: relative;"/>-->
    <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOgAAABWCAYAAADFadp9AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyRpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMy1jMDExIDY2LjE0NTY2MSwgMjAxMi8wMi8wNi0xNDo1NjoyNyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNiAoTWFjaW50b3NoKSIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDo4MjU4MURCMzE5REIxMUU1OUMzRUMzQkRDN0FGNDU5NiIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDo4MjU4MURCNDE5REIxMUU1OUMzRUMzQkRDN0FGNDU5NiI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOjgyNTgxREIxMTlEQjExRTU5QzNFQzNCREM3QUY0NTk2IiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOjgyNTgxREIyMTlEQjExRTU5QzNFQzNCREM3QUY0NTk2Ii8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+Mupy9wAAEqRJREFUeNrsXQuYVVUVXjMDM7wEQUxAHvEQHwlIiqIRGb4QsbHygZqamGSaIpqaJqiA+IxIHUPBIskM0xIxzTAkURxJVCJAGgQC5SEgDAzIMK/+f86+cbic57333Aez/vnWd+6cs88+Z5+z/rPW3nvtvfPq6urEjry8PEknSotLumJzIqQP5EgI/28HaQMpsiWthmyBfAZZDSmDLIYsgCzrP/O6OlEochjxXKznY7oJCkI2xeZsyFDImZDDU5DtVshsyKuQF0HWbfq6FUrQcMQ8GZurIRdCmkdYzj2QVyBTIH8FWWv11SuUoM6kZGbFkJ9CTspAmf8D+TnkNyBqlaqAQgm6l5ynYjMR0jcLyv4x5E7IDK2rKho0QUFMNvA8ArkgC5/BPMgIkPQjVQdFgyMoyDkMm19BDs7i58A66mjIw1o/VTQIgoKYTbApgQzPoefBVt+LQdItqhqKbCVofgrqmu2N6zg8x57HGZD3cf+9VDUU2Yr8JMnJwIJSyAk5Wv7OkLdQjm+qKiiyEQm7uMbyzIG0dUtTXVcr5Xu+CJRfs0aF0rSgcaib3161W6pqa/wLiTK1KWzmlYQ3eSHc3ZdVJRQ5XwcFOXti87YXOYlPdm2Texe/Fujmzjn8KzK047GhCjRx2Rwp277JNx2JP/GE7/gl283bAEnnqKoocrYOarpRZvuRMwfBhq6ZKN9xqiqKbEGjkORk8PpLpu7mi3ZNW8r444Z6pnlm1XuyrHxDUoUY1K5nvXi5uAHRAjIL5TwBlnSjqocipwgKPArpFzjzvHw5pMg77LYwvyDpQtCF9btOCHSEzABJTwNJa1RFFJlEYBcXCnu+WMHuDQHfgNyh6qHICYKCnF/C5okG9mzuQrn7qooocsGCThJrAHVDAn3vqSBpvqqJImsJCgUdgM3FDfT5fBVylaqJIpst6MMN/BmNNbNAKBTZRVAo5hDJzEDrbAL7fUeqqigyAb9ulqRaMhnqt3bn1vqQPDeUV1mhgBt375BFWz8NlX9F1R7fc5sVFErnFq2lKL9RMkW5AR+rif1nXrdHVUaRTriG+kEhWf9amEimlbXV8sqnS+UfG8uksqY644UsyM+Xfm06y7c795GWjZskms3lIOh0VRlFVHAK9fMyK9ckcpGKqkqZuOwNWf9FuSG8SNNW8KUbpb/AtbUiu3EbNTW1Urp5tSwp3yCjjj5V2vOGwmMERAmqyLwFNSF9nH+2ZdgMJy2bK8u3bxQOTDn+ApEjB4l4DySJFjVwSleWiix4BmTdIdK2qIWM6T1YGicWwdQDVvRjVRtFuiyoWyPRmYmQk8SkEKePEuk1NLPkrHdvC0WOGCgyZLT1e3Nlhby9aWWi2V2gaqRIJ9wIOjSRzJZus4Le2x8j0jHLxoS07ijSc+C+95kAhqjKKLKBoGclklmstZZkyEa06WJtYy3HCeAUuP8tVW0UGSMoFLATNl2SqthmaXBcbMKG2rqEp8ZlxfVkVRtFJi3oSfpYPKHPR5FRgvZOx4Vrq0U+KxPZsSnnnllvVRtFuuDUO3lU1Bf9YpvIy2NRZzVtNeyOOe7bwc/f9bnInEdEBowQObiDte+9GSItDsHNnx75MztK1UaRSQv65agvuviVveQk3n8BpN0eoiJYaG1fvltk43Lk9xeRRTNFGqcnpP3LqjaKTBK0Q+QWtHzf/+tqRSorgp9f1ELk7Dus7pxXxltBCCdfIdL9a2l5Zs1Li0taqOooMuXiHhL1RbufIrJinu2CsEmt2ofLg1aUhFz9T0Py9K5Zxmfk+EkpKGgE512+ZdvFdIyM4JqlL9TUVFeZdFNl74Tfp0H+KNageAYvD0a6zT55w3+on5olyKTb8DHkHPP7IuS9XFU/dwnaJOqLduwjctatIh+/I9LsYJHe51oxu2GwfqnI3MdEjjkDdc+2IqVPi+zcInLiJTiYF/lz8+oL5YyHfeL20bZfBlkCkp0HgqzA7x62dJyc7G+Q+8z/P4OMiiMn8+U6q+wsWmsI/32HazlhoS1doIrA1cc9wPxvnfLhbf8OkJZzVXVC2jFx++HjCCck/or58OCtyfNIVxaX7hfYrMD+Ep/r0Lsbi3Q/MP8fis0NEE4q0NrnNkeZj1uYVQTW4VpDHO6Di4NdCjmV5XbgzGSz9Ytn5zP5O+RBXGdLUIKmBYw0SibaaOV8kS6wP/2vsMjdHDbtnWkg7Jkg7KGR337QQN7/mCpDzCWmos4F2ZzWg+GSjRx3yvGn1yDNRBB5re34WENO4h4c24M0/68lQLxq8YnMTkhyXU6SBkhLRVxsU2AGeD4OYdMfZ+v/AMKWA3ZR3YPj+JzK9VDKWMRIV8gIfhSwb5XHdQrNh43XoM/1D8iLYg2LpIJ7DQdkbPkSyC8djvFjNMzhGVY7kBMaJw+JtarCC5Ayh+e7wTyTGWKt9udKA8iVkPnIlx/xzVlD0GRxynDLrY1Z3m4nw3R9FQUqSsvlK4MkAomOBIl4h/3NS+VLOBxys0PaXUg7wRCVX2QukTjCWE+2HH/PJGWw/m/jTl+D810br3D+jQmW80Iozk9BmloPq8b1efrGCIr/qVOzIDtYm8G5m+PSd8fmWchL+D0Yx2PK/byxPEGj2Lhy+qM4/9EQ5dnlUgYSew3y2uZjwe81HsFpSLvYJ219c4tPnjx2M9Ly43IP5LogjUQ7coGgjFaKH5CSJnLWP/igCUGcOsg7rFfSMze73WKdnzDuKzEcxOoRs5g2q30n8kvXINtlAVzCiyBv2f6/0dzr+fHkJLCPHxiuLMdotR/bDpFobaGslwZwqfkBG2RzJSMHrjnceAQD/ciZADgp37lOB/KDfmUU+6A87AkgVYVxi8TUW5zS8Es+xuZGjwNJaZ0uNPuoGM+lsZzPQC7xScPjv7f9TyswEkrs+hHBMT6/6+MIyvSsyz4EMvg1VB5mLF5VmsjJaWfp3RTjmikPrUGeu9wMY76Lr65wx55ECGrQ2K1uY8PvTN01Zp1+bTs2BiRO56rgrN8NgYIWuSguPx47IR+Z/7uxPgyFWxQgbzaKFeGcw22K+r75KPhNVMdRGelcG+hKp8atFH4A8mztFL4EXakc9MR/+8+8LjRJYAlbGbesvs7oYWlJ3rtinjwk1pT2LmRmOgsKhaTVnyt7u2jiMSzOerKxZ3nAvNmwRSJ3izvEsg+E0g7yOJeDjnchzdfT9CiKzYczKpxjGrACEVRnDPDGqhCkPBrSD3KZ7LuW6qs+p7L178O4faNZn3VJ3wHX+NBBvhWVm2u++heZBp8YDoKEcQG3SFyLuHH32AI62dQ13XAnr400/aJ82cifHGHr+3sR5c8GRLb0jnc67tSK+5Fy0BNhGgiWOuxbbxpEBno1LIFcT5l09VYb+2b7uM5O/aGpWA2AC7w+AUVqZeqOMZzCjxX2rcMx+9JySbvgyHM28uTK7WzJ/plLmheRhh+EV7Etd6nD0ZqPC9KX6wH2edPFf920zDphA64xzGH/j3HOeS7nME92wbEePRznz3/SoUfLiaDzo9Tuj98W2bp2331N4PwdlEDf5a6tVnCCHa06WFOcRIgFSZzLfrsRINsmWx+mk+WldfqebVcX7Dsd573uxmnIJw77K1JAliooGZecZAvmNNuhi+Pc21TjJsi/cO1n3QiG/dN5HD/by/5907S+7BJ503TnJPretps6740+7RJO+LPxQJx4x+gu9jP/zbj7EtSCLjONIK2ieOrLXrcC3KMCwwYjJmhpiLRU6M2m0efNECF2rPPEjzsdD5L+3cXN/cSrHzRFbu6YGEGh8AXmHkfHpat0a+xI4MOwGde5jT/Zie/WF2tai9e6ZDMB57LKxgilfgneRy3yYODBNvxeHfL0T3HOhy6uLQNPrsZxzyXo9yMoG0BKi0ve8mgYSAr94Ai8fI/Idx+G5uLRzZsicunkxCYXY5D9s9eigjBE5KhB+Jz/yAobjNIBwPNZEzQxSHNlAo1JrPOMs+1aYupAJxlSvJgBtx5+j/SAUrWDQlFZOahvIX5vjUvHHoB2IfLtJg7RM3EWkp7EjwzJEgFjnNl10xn5rUkwjzeMNZ6YwmfKPu+RuK8Tvax7vodpjgSH9bTC8la+bcXiduqb+Mx/DFbo2t8K+1u1wJp7t8vxkSrqS2kgA+syx5rf7PO8wnZsnCFwWmFcsOdMo1DsHn/vUuc+Bkrna0VNOCDLucInKck52t4dE9YC0lUWK6wuUUw1ZCpM4TOl5WeI4gNe6dxe9qxUVPadWWWNZimbJ7JuMX4nOcMPR7R8ju/i4llWbG5BYaS6Gmk3B8hHj2asrV7Jfs+Ftg/msTaSpBvsZrjE9ImebnQkXukYYcXg7+sD5Mc0r+Gc3T6KzG4/hvU9lsS9s/V8axJkWmis6EMpfqaM5W3KOnIogsKNo6syJ6o33Q0ErdhsWTzGzyaDL/WwRrNwOl7mGyHoHs2LmAR0ibub39NtdVZ2KdTZrGjaY6hNQw1HwnD0yBu2QPd43A25HUp3tof15LzLt8jeqCk/cLRLF+NmhoKJ/WVrabItHxwNM4jxuKbrJVWeCZ/D/W55er3oyeZLmfqGnC7W+M9Du6fA4uVZgfLL8TnpGO1sQVMTCVAIYT2b2Bpd2PR+l60uuxTHacEuMwQmkafYTm+H43M9srd3nU1FWrfW3Q241jAfKzre1IXdlG4JlI3DwV4wo1YeM+SgS9fLuKy8xsVBI3PoDpohbTQaH4QkJ63UXV4B/wHvYasJjKBrX2oaef7qFdIYMN95yIuNXJeIQzBEIx93jkuGBfb9Y8spBJkdofsA+B1dU6PcdHP37Nw/eD4esS6ZprH5N4OD7uZTERspds7HYnQngyjxDRpjjWLz5keDZE/bjtHt/IZH3utsv71q6f/1uUdek90Ns32U7jkoHaOEGOTPIfWFpspEzfgT78G4rqHcTOTJuuDx2DK6apJHcl6P40NbGnL+JkUWbxuuzcbT7xrLx0CJjbJ/d1bYIH6O850WiqCwFtWlxSXsKL8/6FU6NbfGzK79wCJpkUdTwRH4FjVrnRrNbtMZn2af1ttaUGzlO/veZ5j6F57HuoBpGQkTJFa3wpaOBLvW/E/X8b79vhA11StASr74y43icbsz4LV2B0xnHw85wEFBN0Ahe9mGiMXArqd/x6Wl1RxmGlbamXvY5NLnx2F12wLc3+1iBV8w7fd90tKyrXe4VycMEe/xtPFuKYfGPW+6mw4zHwQ7YmWpDZgnvY4Bjg6i2/KD9U+9uKS5aWUL1HS+q3qPjFn0F9mJbbujRU4bKdIkC+Zhr65E5fFJi6D5KN+dvQZL+6aBb4wv+CgQdIUoFBHCafEkT4IaknJIUOBBsR98/olMKZsvdfjj+MyuJ1nRPY0ap7/AtJqcd3f1gr0TlRV36iWDOxwTyrMBOUeo+igyQdAgrYFPmop9IK3u26ajXNNzgExfuUAqKiul7M3sKDxX2D6vc2859bAjwpz2uSS5yrhCkQx8LaixouytZDRJ4Om4uLL2e1vWyPLtn9UvqlQrtRkp4EGNmkj3g9rKiW27SIvwUy6MgPWcomqiyFoX10ZSzpVzfQN6XmypPAsErVPVUWSKoGE6XBm4vKiBPCuOabxcyanINAITFMrK5n/2/5Qf4M+EvvhlKO8GVQ9FzhDUkJRDdxjxUHMAP5MbUM7XVDUUOUdQQ1JO8nvVAfo8JqF8JaoWipwlqCEpJ07+yQH2LBilc5OqhCLnCWpIyiFAPxTJUP9JasFlCq7VRiFFtiFwN4sbSotLzhcryLcoB8vPj8vtIOaDqgqKTCPZbhY3S8rAYY7EXJ1jz4PB6t9RcioOSBc3jqScEZzDmGblSLk5rqUP7numqoDigHZxHVxeLn7DsXpts7C8HPLEFaru53A6ff2KbHdxU05QQ1LOcsuR9+yOKciS8tO6jwQxV6kqKBo0QW1E5dARTiHJ1bnyMlRuBh1MADHfVBVQKEGdicrJTTiulHPptE5DWRmWyPlQHwcx39VXr1CCBiMqZ4Xj9BLnm20q51tg/ZJTI7Lh5w8gZrm+coUSNHGyst+ULb8DjCQzJ/zdkIdASl18WKEEjYiwXARnWAKncrKqvtoqqzgQCZqfRfd3m3FTw2KUklNxoCJrCGoWJXog5Gkzcd7r+hoVStD0gGF3nwZMyzUZb9ZXqFCCps+KspHnloDJJ5kB5AqFEjSN+INYsbJe4HT79+rrUyhB029F2ZQ1Uvau5uWEO5Buu74+hRI0MyTlgjtPuxzmyJlp+uoUStDMggvlOK2TNjLKZQAVCiVoMCu6HpsJcbtnYP9b+toUStDsAFdWjg0PYxDDrfrKFErQ7LGiJGVs9sAHTTCDQqEEzSKSckVmTkr2gL4uRUPD/wQYADLVSxN2O/8tAAAAAElFTkSuQmCC"
         style="float: right; width: 150px; height: auto; position: relative;"/>
</div>
<div class="row">
    <div class="row">
        <div class="col-md-6">
            <h4><strong>Comprobante de la operación: <?php echo $transaction['pr']['tx']; ?></strong></h4>
        </div>
    </div>
    <div class="col-md-12">
        <h4><strong>Datos del pedido</strong></h4>
        <table class="">
            <tr>
                <td class="table-title"><strong>Producto</strong></td>
                <td><?php echo $transaction['transaction']['item']; ?></td>
            </tr>
            <tr>
                <td class="table-title"><strong>Cantidad</strong></td>
                <td><?php echo ($transaction['it']['quantity'] * $transaction['transaction']['sets']); ?></td>
            </tr>
            <tr>
                <td class="table-title"><strong>Sets</strong></td>
                <td><?php echo $transaction['transaction']['sets']; ?></td>
            </tr>
            <tr>
                <td class="table-title"><strong>Acabado</strong></td>
                <td><?php echo $transaction['transaction']['finish']; ?></td>
            </tr>
            <tr>
                <td class="table-title"><strong>Sub Total</strong></td>
                <td>$<?php echo number_format($transaction['transaction']['subtotal'], 2); ?></td>
            </tr>
            <tr>
                <td class="table-title"><strong>Envío</strong></td>
                <td>$<?php echo number_format($transaction['transaction']['deliveryprice'], 2); ?></td>
            </tr>
            <tr>
                <td class="table-title"><strong>IVA</strong></td>
                <td>$<?php echo number_format($transaction['transaction']['iva'], 2); ?></td>
            </tr>
            <tr>
                <td class="table-title"><strong>TOTAL</strong></td>
                <td>$<?php echo number_format($transaction['transaction']['iva'] + $transaction['transaction']['deliveryprice'] + $transaction['transaction']['subtotal'], 2); ?></td>
            </tr>
            <tr>
                <td class="table-title"><strong>Comprador</strong></td>
                <td><?php echo $transaction['buyer']['name']; ?></td>
            </tr>
            <tr>
                <td class="table-title"><strong>Dirección</strong></td>
                <td><?php echo $transaction['buyer']['address']; ?></td>
            </tr>
            <tr>
                <td class="table-title"><strong>Correo</strong></td>
                <td><?php echo $transaction['buyer']['email']; ?></td>
            </tr>
            <tr>
                <td class="table-title"><strong>Telefóno</strong></td>
                <td><?php echo $transaction['buyer']['phone']; ?></td>
            </tr>
        </table>
    </div>
    <?php if ($transaction['delivery']['needdelivery']) { ?>
        <div class="col-md-12">
            <h4><strong>Datos del envío</strong></h4>
            <table class="">
                <tr>
                    <td class="table-title"><strong>Destinatario</strong></td>
                    <td><?php echo $transaction['delivery']['name']; ?></td>
                </tr>
                <tr>
                    <td class="table-title"><strong>Dirección</strong></td>
                    <td><?php echo $transaction['delivery']['address']; ?></td>
                </tr>
                <tr>
                    <td class="table-title"><strong>Colonia</strong></td>
                    <td><?php echo $transaction['delivery']['colony']; ?></td>
                </tr>
                <tr>
                    <td class="table-title"><strong>Ciudad</strong></td>
                    <td><?php echo $transaction['delivery']['city']; ?></td>
                </tr>
                <tr>
                    <td class="table-title"><strong>Estado</strong></td>
                    <td><?php echo $transaction['delivery']['state']; ?></td>
                </tr>
                <tr>
                    <td class="table-title"><strong>CP.</strong></td>
                    <td><?php echo $transaction['delivery']['postcode']; ?></td>
                </tr>
                <tr>
                    <td class="table-title"><strong>Telefóno</strong></td>
                    <td><?php echo $transaction['delivery']['phone']; ?></td>
                </tr>
                <tr>
                    <td class="table-title"><strong>Referencias</strong></td>
                    <td><?php echo $transaction['delivery']['references']; ?></td>
                </tr>
            </table>
        </div>
    <?php } else { ?>
        <div class="col-md-12">
            <h4><strong>Datos del envío</strong></h4>
            <h4>-----La orden será entregada en nuestras oficinas------</h4>
        </div>
    <?php } ?> 
    <?php if ($transaction['design']['need_design']) { ?>
        <div class="col-md-12">
            <h4>Contenido de las tarjetas</h4>
            <table class="">
                <tr>
                    <td class="table-title"><strong>Nombre</strong></td>
                    <td><?php echo $transaction['design']['name']; ?></td>
                </tr>
                <tr>
                    <td class="table-title"><strong>Dirección</strong></td>
                    <td><?php echo $transaction['design']['address']; ?></td>
                </tr>
                <tr>
                    <td class="table-title"><strong>Compañía</strong></td>
                    <td><?php echo $transaction['design']['company_name']; ?></td>
                </tr>
                <tr>
                    <td class="table-title"><strong>Cargo</strong></td>
                    <td><?php echo $transaction['design']['charge']; ?></td>
                </tr>
                <tr>
                    <td class="table-title"><strong>Telefóno</strong></td>
                    <td><?php echo $transaction['design']['phone_1']; ?></td>
                </tr>
                <tr>
                    <td class="table-title"><strong>Celular</strong></td>
                    <td><?php echo $transaction['design']['cellphone']; ?></td>
                </tr>
                <tr>
                    <td class="table-title"><strong>Facebook</strong></td>
                    <td><?php echo $transaction['design']['facebook']; ?></td>
                </tr>
                <tr>
                    <td class="table-title"><strong>Twitter</strong></td>
                    <td><?php echo $transaction['design']['twitter']; ?></td>
                </tr>
                <tr>
                    <td class="table-title"><strong>Instagram</strong></td>
                    <td><?php echo $transaction['design']['instagram']; ?></td>
                </tr>
                <tr>
                    <td class="table-title"><strong>Correo</strong></td>
                    <td><?php echo $transaction['design']['email']; ?></td>
                </tr>
                <tr>
                    <td class="table-title"><strong>Comentarios</strong></td>
                    <td><?php echo $transaction['design']['comments']; ?></td>
                </tr>
            </table>
        </div>
    <?php } else { ?>
        <div class="col-md-12">
            <h4><strong>Contenido de las tarjetas</strong></h4>
            <h4>-----Diseño porporcionado por el cliente-----</h4>
        </div>
    <?php } ?> 
    <footer class="footer">
        <div class="container">
            <p class="text-muted">Boulevard Cristóbal Colón 5-216
                Torre Ánimas
                CP. 91190 Xalapa, Ver. 
                Tel. (228) 8128012. <br>
                Horario de atención: Lunes a Viernes 10am - 3pm y 5 pm - 8pm.<br>
                <strong class="text-magenta">www.pixelmonster.mx</strong>
            </p>
        </div>
    </footer>
</div>
<?php
require_once $_SESSION["base_path"] . "partials/footer.php";
?>
