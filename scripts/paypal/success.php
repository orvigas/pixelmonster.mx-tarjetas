<?php
session_start();
/**
 * Created by PhpStorm.
 * User: orlando
 * Date: 03/10/2016
 * Time: 04:40 PM
 */
require_once $_SESSION["base_path"] . "partials/header.php";

if (!(isset($_GET['tx']) && isset($_GET['st']) && isset($_GET['amt']) && isset($_GET['cc']) && isset($_GET['item_number']))) {
    echo '<br><br><div class="col-md-12 text-center"><h1>¡Datos incorrectos!</h1></div>';
    return null;
}

$db = new TransactionDB();
$transaction = $db->insertPaymentReference($_GET['tx'], $_GET['amt'], $_GET['cc'], $_GET['st'], (int) $_GET['item_number']);
sendMail($transaction['buyer']['email'], $transaction['buyer']['name'],$_GET['tx'],1);
?>
<style>
    table {
        font-size: 11px;;
    }
</style>
<div class="row">
    <div class="col-md-12 text-center">
        <h1>¡Tu pedido se realizó exitosamente!</h1>
        <h4>En un periodo no mayor a 24 horas hábiles nos pondremos en contacto contigo para dar seguimiento a tu proyecto.</h4>
    </div>
    <div class="col-md-8 col-md-offset-2">
        <br><br>
        <div class="row">
            <div class="col-md-6">
                <fieldset class="f-set">
                    <legend>Datos del pedido</legend>
                    <table class="table table-condensed table-bordered table-striped">
                        <tr>
                            <td><strong>Producto</strong></td>
                            <td><?php echo $transaction['transaction']['item']; ?></td>
                        </tr>
                        <tr>
                            <td><strong>Cantidad</strong></td>
                            <td><?php echo ($transaction['it']['quantity'] * $transaction['transaction']['sets']); ?></td>
                        </tr>
                        <tr>
                            <td><strong>Sets</strong></td>
                            <td><?php echo $transaction['transaction']['sets']; ?></td>
                        </tr>
                        <tr>
                            <td><strong>Acabado</strong></td>
                            <td><?php echo $transaction['transaction']['finish']; ?></td>
                        </tr>
                        <tr>
                            <td><strong>Sub Total</strong></td>
                            <td>$<?php echo $transaction['transaction']['subtotal']; ?></td>
                        </tr>
                        <tr>
                            <td><strong>Envío</strong></td>
                            <td>$<?php echo $transaction['transaction']['deliveryprice']; ?></td>
                        </tr>
                        <tr>
                            <td><strong>IVA</strong></td>
                            <td>$<?php echo $transaction['transaction']['iva']; ?></td>
                        </tr>
                        <tr>
                            <td><strong>TOTAL</strong></td>
                            <td>$<?php echo ($transaction['transaction']['iva'] + $transaction['transaction']['subtotal'] + $transaction['transaction']['deliveryprice']); ?></td>
                        </tr>
                        <tr>
                            <td><strong>Comprador</strong></td>
                            <td><?php echo $transaction['buyer']['name']; ?></td>
                        </tr>
                        <tr>
                            <td><strong>Dirección</strong></td>
                            <td><?php echo $transaction['buyer']['address']; ?></td>
                        </tr>
                        <tr>
                            <td><strong>Correo</strong></td>
                            <td><?php echo $transaction['buyer']['email']; ?></td>
                        </tr>
                        <tr>
                            <td><strong>Telefóno</strong></td>
                            <td><?php echo $transaction['buyer']['phone']; ?></td>
                        </tr>
                        <tr>
                            <td><strong>Ref. pago</strong></td>
                            <td><?php echo $_GET['tx']; ?></td>
                        </tr>
                    </table>
                </fieldset>
            </div>
            <div class="col-md-6">
                <fieldset class="f-set">
                    <legend>Datos del envío</legend>
                    <?php if ($transaction['delivery']['needdelivery']) { ?>
                        <table class="table table-condensed table-bordered table-striped">
                            <tr>
                                <td><strong>Destinatario</strong></td>
                                <td><?php echo $transaction['delivery']['name']; ?></td>
                            </tr>
                            <tr>
                                <td><strong>Dirección</strong></td>
                                <td><?php echo $transaction['delivery']['address']; ?></td>
                            </tr>
                            <tr>
                                <td><strong>Colonia</strong></td>
                                <td><?php echo $transaction['delivery']['colony']; ?></td>
                            </tr>
                            <tr>
                                <td><strong>Ciudad</strong></td>
                                <td><?php echo $transaction['delivery']['city']; ?></td>
                            </tr>
                            <tr>
                                <td><strong>Estado</strong></td>
                                <td><?php echo $transaction['delivery']['state']; ?></td>
                            </tr>
                            <tr>
                                <td><strong>CP.</strong></td>
                                <td><?php echo $transaction['delivery']['postcode']; ?></td>
                            </tr>
                            <tr>
                                <td><strong>Telefóno</strong></td>
                                <td><?php echo $transaction['delivery']['phone']; ?></td>
                            </tr>
                            <tr>
                                <td><strong>Referencias</strong></td>
                                <td><?php echo $transaction['delivery']['references']; ?></td>
                            </tr>
                        </table>
                    <?php } else { ?>
                        <div class="col-md-12 text-center">
                            <h4 class="text-success">Has elegido recoger tu pedido en nuestras oficinas.</h3>
                                <p class="text-muted">Dirección: Boulevard Cristóbal Colón 5-216
                                    Torre Ánimas
                                    CP. 91190 Xalapa, Ver. 
                                    Tel. (228) 8128012. <br>
                                    Horario de atención: Lunes a Viernes 10am - 3pm y 5 pm - 8pm.</p>
                        </div>
                    <?php } ?>
                </fieldset>
            </div>
        </div>
        <?php if ($transaction['design']['need_design']) { ?>
            <div class="row">
                <div class="col-md-12">
                    <fieldset class="f-set">
                        <legend>Contenido de las tarjetas</legend>
                        <div class="col-md-8 col-md-offset-2">
                            <dl class="dl-horizontal">
                                <dt>Nombre</dt>
                                <dd><?php echo $transaction['design']['name']; ?></dd>
                                <dt>Dirección</dt>
                                <dd><?php echo $transaction['design']['address']; ?></dd>
                                <dt>Compañía</dt>
                                <dd><?php echo $transaction['design']['company_name']; ?></dd>
                                <dt>Cargo</dt>
                                <dd><?php echo $transaction['design']['charge']; ?></dd>
                                <dt>Telefóno</dt>
                                <dd><?php echo $transaction['design']['phone_1']; ?></dd>
                                <dt>Celular</dt>
                                <dd><?php echo $transaction['design']['cellphone']; ?></dd>
                                <dt>Facebook</dt>
                                <dd><?php echo $transaction['design']['facebook']; ?></dd>
                                <dt>Twitter</dt>
                                <dd><?php echo $transaction['design']['twitter']; ?></dd>
                                <dt>Instagram</dt>
                                <dd><?php echo $transaction['design']['instagram']; ?></dd>
                                <dt>Correo</dt>
                                <dd><?php echo $transaction['design']['email']; ?></dd>
                                <dt>Comentarios</dt>
                                <dd><?php echo $transaction['design']['comments']; ?></dd>
                            </dl>
                        </div>
                    </fieldset>
                </div>
            </div>
        <?php } ?>
    </div>
    <div class="col-md-12 text-center">
        <p>
            <button onclick="printComp()" class="btn btn-danger"><i class="fa fa-fw fa-file-pdf-o"></i> Descargar
                comprobante
            </button>
        </p>
    </div>
</div>

<script type="text/javascript">
    function printComp() {
        window.open('<?php echo $_SESSION['base_url']; ?>scripts/paypal/pdf.php?transaction=<?php echo $_GET['item_number']; ?>', '<?php echo $_GET['tx']; ?>', "height=800,width=600,location=0,scrollbars=1,toolbar=0");
                //window.location = "<?php echo $_SESSION['base_url']; ?>";
            }
</script>

<?php
require_once $_SESSION["base_path"] . "partials/footer.php";
?>
