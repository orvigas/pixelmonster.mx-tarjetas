<?php

session_start();
date_default_timezone_set('America/Mexico_City');

use Dompdf\Dompdf;

require_once $_SESSION["base_path"] . "vendor/autoload.php";
if (!(isset($_GET['transaction']))) {
    echo '<br><br><div class="col-md-12 text-center"><h1>¡Datos incorrectos!</h1></div>';
    return null;
}
$tr = (integer) $_GET['transaction'];

//$html = file_get_contents("http://$_SERVER[HTTP_HOST]"."/sistema-tarjetas/scripts/paypal/success_PDF.php?transaction=$tr");

function get_content($URL) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_URL, $URL);
    $data = curl_exec($ch);
    curl_close($ch);
    return $data;
}

$html = get_content("http://$_SERVER[HTTP_HOST]" . "/sistema-tarjetas/scripts/paypal/success_PDF.php?transaction=$tr");
// instantiate and use the dompdf class
$dompdf = new Dompdf();
$dompdf->loadHtml($html);
// (Optional) Setup the paper size and orientation
$dompdf->setPaper('letter', 'portrait');
// Render the HTML as PDF
$dompdf->render();
// Output the generated PDF to Browser
$dompdf->stream("TRANSACTION_" . $_GET['transaction'] . ".pdf", array("Attachment" => false));
