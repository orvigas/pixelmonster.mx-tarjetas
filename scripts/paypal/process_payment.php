<?php

session_start();
/**
 * Created by PhpStorm.
 * User: orlando
 * Date: 03/10/2016
 * Time: 04:40 PM
 */
// Report simple running errors
error_reporting(0);
require $_SESSION["base_path"] . "scripts/autoload.php";

$db = new TransactionDB();
/* * Capturar parámetros de entrada vía POST* */
$params = json_decode(file_get_contents('php://input'), true);
$buyer = $params['buyer'];
$cardContent = $params['buy']['card_content'];
$params['buy']['delivery']['needdelivery'] = $params['buy']['needdelivery'];
$delivery = $params['buy']['delivery'];
$row = $db->insertTransaction('TARJETAS DE PRESENTACIÓN', $params['buy']['item'], $params['buy']['sets'], $params['buy']['cardfinish'], $params['buy']['subtotal'], $params['buy']['iva'], $params['buy']['deliveryprice']);
$db->insertDesign($cardContent['name'], $cardContent['address'], $cardContent['company'], $cardContent['charge'], $cardContent['office_phone'], null, $cardContent['cell_phone'], $cardContent['facebook'], $cardContent['twitter'], $cardContent['instagram'], $cardContent['email'], $cardContent['comments'], $cardContent['needdesign'], $row['id']);
$db->insertBuyer($buyer['nombre'], $buyer['direccion'], $buyer['telefono'], $buyer['correo'], $row['id']);
$db->insertDelivery($delivery['person'], $delivery['address'], $delivery['colony'], $delivery['city'], $delivery['state'], $delivery['postcode'], $delivery['phone'], $delivery['reference'], $delivery['needdelivery'], $row['id']);
$transaction = $db->getTransaction($row['id']);
//header('Content-Type: application/json');
$transaction['transaction']['id'] = str_pad($row['id'], 9, '0', STR_PAD_LEFT);
sendMail($buyer['correo'], $buyer['nombre'],NULL,0);
echo json_encode($transaction);
$db->close();
