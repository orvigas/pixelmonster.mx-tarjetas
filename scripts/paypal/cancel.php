<?php
session_start();
require_once $_SESSION["base_path"] . "partials/header.php";
?>

<div class="row">
    <div class="col-md-4"></div>
    <div class="col-md-4 text-center">
        <h1>Pago cancelado!!</h1>
    </div>
    <div class="col-md-4"></div>
</div>

<?php
require_once $_SESSION["base_path"] . "partials/footer.php";
?>