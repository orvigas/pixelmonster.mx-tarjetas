<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

session_start();

date_default_timezone_set('America/Mexico_City');

require_once $_SESSION["base_path"] . "vendor/autoload.php";

require $_SESSION["base_path"] . 'vendor/phpmailer/phpmailer/PHPMailerAutoload.php';

function sendMail($e, $n,$payment,$opc) {
    $mail = new PHPMailer;
    $mail->CharSet = 'UTF-8';
//$mail->SMTPDebug = 3;                               // Enable verbose debug output
//$mail->isSMTP();                                      // Set mailer to use SMTP
    $mail->Host = 'hv2svg036.neubox.net';  // Specify main and backup SMTP servers
    $mail->SMTPAuth = true;                               // Enable SMTP authentication
    $mail->Username = 'pagos@pixelmonster.mx';                 // SMTP username
    $mail->Password = 'r1qcYvlqhv';                           // SMTP password
    $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
    $mail->Port = 456;                                    // TCP port to connect to

    $mail->setFrom('pagos@pixelmonster.mx', 'pixelmonster.mx');
    $mail->addAddress($e, $n);     // Add a recipient
//$mail->addAddress('ellen@example.com');               // Name is optional
//$mail->addReplyTo('info@example.com', 'Information');
//$mail->addCC('cc@example.com');
//$mail->addBCC('bcc@example.com');
//$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
//$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
    $mail->isHTML(true);                                  // Set email format to HTML

    if($opc == 0){
        $mail->Subject = 'Tus datos de compra en Pixel Monster';
    $mail->Body = '<h3><strong>¡Realizaste tu pedido exitosamente!</strong></h3>'
            . '<br>Una vez confirmado tu pago, recibirás un correo de confirmación del mismo.<br><br>¡Gracias por preferirnos!</br>'
            . '<br><br><img src="http://pixelmonster.mx/wp-content/uploads/2015/07/default-logo-small-2x.png" class="small" alt="" style="display: inline; float:left; width:150px;"><br><br>'
            . '<br><br>El equipo de Pixel Monster<br>
               Boulevard Cristóbal Colón 5-216<br>
               Torre Ánimas<br>
               CP. 91190 Xalapa, Ver.<br>
               Tel. (228) 8128012.<br>
               www.pixelmonster.mx';
    $mail->AltBody = 'Una vez confirmado tu pago, recibirás un correo de confirmación del mismo. ¡Gracias por preferirnos!';
    }else{
        $mail->Subject = 'Tus datos de compra en Pixel Monster';
    $mail->Body = '<h3><strong>¡Realizaste tu compra exitosamente!</strong></h3>'
            . '</br><strong>REFERENCIA PAYPAL: '.$payment.'</strong><br>'
            . '<br>En un periodo no mayor a 24 horas hábiles te estaremos contactando para dar seguimiento a tu proyecto.<br><br>Toma en cuenta que nuestros horarios de oficina son los siguientes:<br><br>Lunes a viernes 10am-3pm y 5pm a 8pm.<br><br>En breve un asesor se pondrá en contacto contigo para realizar tu diseño o si mandaste el tuyo, asesorarte antes de mandar a producción por si hay que hacer algún cambio.<br><br>¡Gracias por preferirnos!<br>'
            . '<br><br><img src="http://pixelmonster.mx/wp-content/uploads/2015/07/default-logo-small-2x.png" class="small" alt="" style="display: inline; float:left; width:150px;"><br><br>'
            . '<br><br>El equipo de Pixel Monster<br>
               Boulevard Cristóbal Colón 5-216<br>
               Torre Ánimas<br>
               CP. 91190 Xalapa, Ver.<br>
               Tel. (228) 8128012.<br>
               www.pixelmonster.mx';
    $mail->AltBody = '¡Realizaste tu compra exitosamente! REFERENCIA PAYPAL: '.$payment.' En un periodo no mayor a 24 horas hábiles te estaremos contactando para dar seguimiento a tu proyecto. Toma en cuenta que nuestros horarios de oficina son los siguientes: Lunes a viernes 10am-3pm y 5pm a 8pm. En breve un asesor se pondrá en contacto contigo para realizar tu diseño o si mandaste el tuyo, asesorarte antes de mandar a producción por si hay que hacer algún cambio. ¡Gracias por preferirnos!';
    }

    if (!$mail->send()) {
        //echo 'Message could not be sent.<br>';
        //echo 'Mailer Error: ' . $mail->ErrorInfo;
    } else {
        //echo 'Message has been sent';
    }
}
