<?php
require_once $_SESSION["base_path"] . "partials/header.php";
?>
<div class="row" ng-init="init()">
    <div class="col-md-12">
        <div class="col-md-12">
            <div id="wizard-container">
                <div id="wizard-step-container">
                    <ul class="nav nav-pills nav-justified nav-wizard">
                        <li ng-repeat="step in steps" ng-class=" ({
        'active':step.step == currentStep})">
                            <div class="nav-wedge" ng-if="step.step != '1'"></div>
                            <a data-toggle="tab" ng-if="currentStep <= steps.length && !formBuy.$valid"
                               href="javascript:void" style="pointer-events: none; cursor: default;">{{step.step}}.
                                {{step.name}}</a>
                            <a ng-click="gotoStep(step.step)" data-toggle="tab" ng-if="formBuy.$valid">{{step.step}}.
                                {{step.name}}</a>
                            <div ng-if="step.step < steps.length" class="nav-arrow"></div>
                        </li>
                    </ul>
                </div>

                <form id="wizard-content-container" action="<?php echo $paypal_url; ?>" method="post" name="formBuy" id="formBuy" enctype="multipart/form-data" target="_top">
                    <div class="form-horizontal" role="form">
                        <!-- Identify your business so that you can collect the payments. -->
                        <input type="hidden" name="business" value="<?php echo $paypal_id; ?>">
                        <!-- Specify a Buy Now button. -->
                        <input type="hidden" name="cmd" value="_xclick">
                        <!-- Specify URLs -->
                        <input type='hidden' name='cancel_return'
                               value='<?php echo $_SESSION['base_url'] . "scripts/tools/redirector.php"; ?>'>
                        <input type='hidden' name='return'
                               value='<?php echo $_SESSION['base_url'] . "scripts/tools/redirector.php"; ?>'>
                        <!-- Specify details about the item that buyers will purchase. -->
                        <input type="hidden" name="item_name"
                               value="{{buy.quantity}} TARJETAS DE PRESENTACION {{buy.cardfinish}}">
                        <input type="hidden" name="item_number" id="item_number">
                        <input type="hidden" name="currency_code" value="MXN">
                        <input type="hidden" name="amount" value="{{buy.subtotal| number:2}}">
                        <input type="hidden" name="shipping" value="{{buy.deliveryprice| number:2}}">
                        <input type="hidden" name="tax" value="{{buy.iva| number:2}}">
                        <!-- Buyer info -->
                        <input type="hidden" name="<first_name>" ng-model="buyer.nombre">
                        <input type="hidden" name="<address1>" ng-model="buyer.direccion">
                        <input type="hidden" name="<night_phone_a>" ng-model="buyer.telefono">
                        <input type="hidden" name="email" ng-model="buyer.correo">
                        <div ng-include src="getStepTemplate()"></div>
                        <div class="hidden" id="file-container"></div>
                    </div>

                    <div id="wizard-navigation-container">
                        <div class="row">
                            <div class="pull-left" ng-if="currentStep == 1 && buy.item">
                                <div class="text-right">
                                    <h5 class="text-magenta"><strong>SUB TOTAL: ${{buy.subtotal| number:2}}</strong></h5>
                                    <h5 class="text-magenta"><strong>ENVÍO: ${{buy.deliveryprice| number:2}}</strong></h5>
                                    <h5 class="text-magenta"><strong>IVA: ${{buy.iva| number:2}}</strong></h5>
                                    <h5 class="text-success"><strong>TOTAL: ${{buy.total| number:2}}</strong></h5>
                                </div>
                            </div>
                            <div class="pull-right" ng-if="buy.item">
                                <span class="btn-group">
                                    <button href="javascript:void(0)" ng-disabled="currentStep <= 1" class="btn btn-default" name="previous"
                                            type="button"
                                            ng-click="gotoStep(currentStep - 1)"><i class="fa fa-arrow-left"></i> Paso anterior
                                    </button>
                                    <button href="javascript:void(0)" ng-disabled="currentStep >= steps.length || !formBuy.$valid"
                                            class="btn btn-primary"
                                            name="next"
                                            type="button" ng-click="gotoStep(currentStep + 1)">Paso siguiente <i
                                            class="fa fa-arrow-right"></i></button>
                                </span>
                                <button id="enviar" ng-disabled="currentStep != steps.length" class="btn btn-success hidden"
                                        name="next"
                                        type="submit"><i class="fa fa-usd"></i> Pagar
                                </button>
                                <a href="javascript:void(0)" ng-if="(currentStep == steps.length && acceptTerms.val && !loading)"
                                   class="btn btn-success" name="next"
                                   ng-click="save()"><img id="btn-paypal" src="images/PayPal_logo.png"/>
                                </a>
                                <a href="javascript:void(0)" ng-if="(loading)"
                                   class="btn btn-info" name="next">
                                   Procesando <img id="btn-paypal" src="images/ajax-loader.gif"/>
                                </a>
                            </div>
                        </div>
                </form>
            </div>
        </div>
    </div>
</div>
</div>
<br><br>
<?php
require_once $_SESSION["base_path"] . "partials/footer.php";
?>